package main

import (
    "os"
    "fmt"
    "math"
    "time"
    "encoding/xml"
    "sort"
    "sync"
    "runtime"
)

type Root struct {
    Items []Item `xml:"Item"`
}

type Item struct {
    Label string
    Value float64
}

type Sequence []float64

// Methods required by sort.Interface.
func (s Sequence) Len() int {
    return len(s)
}
func (s Sequence) Less(i, j int) bool {
    return s[i] < s[j]
}
func (s Sequence) Swap(i, j int) {
    s[i], s[j] = s[j], s[i]
}

func getMin(s []float64) float64 {
    var min float64 = 999.0
    for i:=0; i<len(s); i++ {
        if s[i] < min {
            min = s[i]
        }
    }
    return min
}

func getMax(s []float64) float64 {
    var max float64 = 0.0
    for i:=0; i<len(s); i++ {
        if s[i] > max {
            max = s[i]
        }
    }
    return max
}

func getMedian(s []float64) float64 {
    var n int = len(s)
    if n % 2 != 1 {
        return (s[n / 2] + s[n / 2 - 1]) / 2
    }
    return s[n / 2]
}

func getQ1(s []float64) float64 {
    var n int = len(s)
    if n % 2 != 1 {
        return getMedian(s[:n/2])
    }
    return getMedian(s[:n/2])
}

func getQ3(s []float64) float64 {
    var n int = len(s)
    if n % 2 != 1 {
        return getMedian(s[n/2:])
    }
    return getMedian(s[n/2+1:])
}

func getMean(s []float64) float64 {
    var sum float64 = 0.0
    var n int = len(s)
    for i:=0; i<n; i++ {
        sum += s[i]
    }
    mean := sum/float64(n)
    return mean
}

func getMeanF(s []float64) float64 {
    var sum float64 = 0.0
    var n int = len(s)
    for i:=0; i<n; i++ {
        sum += s[i]
    }
    mean := sum/float64(n)
    return mean
}

func getVariance(s []float64, mean float64) float64 {
    var squares []float64
    for _, value := range s {
        distToMean := value - mean
        squares = append(squares, distToMean * distToMean)
    }
    return getMeanF(squares)
}

func printDone(startTime int64) {
    fmt.Printf("Done at %d second mark\n", time.Now().Unix()-startTime)
}

func sortingWorker(wg *sync.WaitGroup, values []float64) {
    sort.Float64s(values)
    wg.Done()
}

// parallel sort
func pSort(values []float64) {
    l := len(values)
    if l < 2 {
        return
    }

    midPoint := l/2
    firstHalf := values[:midPoint]
    secondHalf := values[midPoint:]

    var wg sync.WaitGroup
    wg.Add(2)

    go sortingWorker(&wg, firstHalf)
    go sortingWorker(&wg, secondHalf)

    wg.Wait()
    result := append(firstHalf, secondHalf...)
    var resultSequence Sequence = result
    sort.Stable(resultSequence)
}

func main() {
    runtime.GOMAXPROCS(3)

    var startTime int64 = time.Now().Unix()

    if len(os.Args) < 2 {
        fmt.Println("Must specify file name")
        os.Exit(1)
    }
    var fileName string = os.Args[1]

    fmt.Print("Reading... ")
    file, err := os.Open(fileName)
    if err != nil {
        panic(err)
    }
    defer file.Close()

    decoder := xml.NewDecoder(file)

    var allValues []float64
    for {
        t, _ := decoder.Token()
        if t == nil {
            break
        }

        switch se := t.(type) {
        case xml.StartElement:
            if se.Name.Local == "Item" {
                var itm Item
                decoder.DecodeElement(&itm, &se)
                allValues = append(allValues, itm.Value)
            }
        }
    }
    printDone(startTime)

    fmt.Print("Sorting Values... ")
    pSort(allValues)
    printDone(startTime)

    fmt.Print("Calculating Statistics... ")

    var n int = len(allValues)
    if n < 1 {
        fmt.Println("ERROR: Length", n)
        return
    }
    
    var min float64 = getMin(allValues)
    var q1 float64 = getQ1(allValues)
    var median float64 = getMedian(allValues)
    var q3 float64 = getQ3(allValues)
    var max float64 = getMax(allValues)

    var mean float64 = getMean(allValues)
    var variance float64 = getVariance(allValues, mean)
    var stdDev float64 = math.Sqrt(variance)

    var con95 float64 = 1.96 * (stdDev / (math.Sqrt(float64(n))))
    var low95 float64 = mean - con95
    var high95 float64 = mean + con95

    var con99 float64 = 2.58 * (stdDev / (math.Sqrt(float64(n))))
    var low99 float64 = mean - con99
    var high99 float64 = mean + con99
    printDone(startTime)

    fmt.Println()
    fmt.Println("=== Five Number Summary ===")
    fmt.Println("Min:", min)
    fmt.Println("Q1:", q1)
    fmt.Println("Median:", median)
    fmt.Println("Q3:", q3)
    fmt.Println("Max:", max)

    fmt.Println()
    fmt.Println("=== Standard Deviation and Related Parameters ===")
    fmt.Println("n:", n)
    fmt.Println("Mean:", mean)
    fmt.Println("Variance:", variance)
    fmt.Printf("Standard Deviation: %f\n", stdDev)

    fmt.Println()
    fmt.Println("=== Confidence Intervals ===")
    fmt.Printf("95%%: %f, %f\n", low95, high95)
    fmt.Printf("99%%: %f, %f\n", low99, high99)
}

