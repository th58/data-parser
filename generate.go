package main

/*
  Generate a large XML file to be statistically analyzed.
Usage: ./generate > output.xml
*/

import (
    "os"
    "fmt"
    "time"
    "math/rand"
    "encoding/xml"
)

type Item struct {
    Label string
    Value float64
}

func randSeq(n int) string {
    var letters = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")

    b := make([]rune, n)
    for i := range b {
        b[i] = letters[rand.Intn(len(letters))]
    }
    return string(b)
}

func main() {
    rand.Seed(time.Now().UTC().Unix())
    size := 10000000
    content := make([]Item, size)
    for i := 0; i < size; i++ {
        newItem := Item{Label: randSeq(10), Value: rand.NormFloat64()}
        content[i] = newItem
    }
    output, err := xml.MarshalIndent(content, "    ", "    ")
    if err != nil {
        panic(err)
    }

    fmt.Println("<root>")
    os.Stdout.Write(output)
    fmt.Println()
    fmt.Println("</root>")
}

